// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//       console.log(data)

//       const selectTag = document.getElementById('conference');
//       for (let conference of data.conferences) {
//         // Create an 'option' element
//         const option = document.createElement('option')

//         // Set the '.value' property of the option element to the
//         // state's abbreviation
//         option.value = conference.id

//         // Set the '.innerHTML' property of the option element to
//         // the state's name
//         option.innerHTML = conference.name

//         // Append the option element as a child of the select tag
//         selectTag.appendChild(option)
//       }
//     }


//     // State fetching code, here...
//     const selectTag = document.getElementById('conference');
//     const formTag = document.getElementById('create-presentation-form');

//     formTag.addEventListener('submit', async event => {
//       event.preventDefault();
//       const formData = new FormData(formTag);
//       const json = JSON.stringify(Object.fromEntries(formData));
//       console.log(json);
//       const conferenceId = selectTag.options[selectTag.selectedIndex].value;
//       console.log(conferenceId)
//       const presentationUrl = `http://localhost:8000/api/conferences/ ${conferenceId}/presentations/`;
//       const fetchConfig = {
//       method: "post",
//       body: json,
//       headers: {
//           'Content-Type': 'application/json',
//       },
//       };
//       const response = await fetch(presentationUrl, fetchConfig);
//       if (response.ok) {
//       formTag.reset();
//       const newPresentation = await response.json();
//       console.log(newPresentation)
//       }
//           });
//       });
//////////////////////

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('conference');
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.id;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }

    const formTag = document.getElementById('create-presentation-form');
    const selectTag = document.getElementById('conference');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const conferenceId = selectTag.options[selectTag.selectedIndex].value;
      console.log(conferenceId)
      const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
      console.log(locationUrl)
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }
    });
  });
// //

// window.addEventListener('DOMContentLoaded', async () => {

//   const url = 'http://localhost:8000/api/conferences/';

//   const response = await fetch(url);

//   if (response.ok) {
//     const data = await response.json();

//     const selectTag = document.getElementById('conference');
//     for (let conference of data.conferences) {
//       const option = document.createElement('option');
//       option.value = conference.id;
//       option.innerHTML = conference.name;
//       selectTag.appendChild(option);
//     }
//   }

//   const formTag = document.getElementById('create-presentation-form');
//   const selectTag = document.getElementById('conference');
//   let conferenceId; // declare conferenceId outside of event listener
//   formTag.addEventListener('submit', async event => {
//     event.preventDefault();
//     const formData = new FormData(formTag);
//     const json = JSON.stringify(Object.fromEntries(formData));

//     conferenceId = selectTag.options[selectTag.selectedIndex].value; // update conferenceId value
//     console.log(conferenceId)
//     const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
//     const fetchConfig = {
//       method: "post",
//       body: json,
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };
//     const response = await fetch(locationUrl, fetchConfig);
//     if (response.ok) {
//       formTag.reset();
//       const newConference = await response.json();
//       console.log(newConference);
//     }
//   });

//   console.log(conferenceId); // will log undefined initially

// });
